<?php

namespace Shakegwapo\SapIcfService\Tests;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use YourNamespace\SapIcfService\SapService;
use PHPUnit\Framework\TestCase;
use Mockery;

class SapServiceTest extends TestCase
{
    protected $sapService;
    protected $mockClient;

    protected function setUp(): void
    {
        $this->mockClient = Mockery::mock(Client::class);
        $this->sapService = new SapService();
        $this->sapService->client = $this->mockClient;
    }

    public function testGetRequest()
    {
        $this->mockClient->shouldReceive('request')
            ->with('GET', 'test-endpoint')
            ->andReturn(new Response(200, [], json_encode(['success' => true])));

        $response = $this->sapService->get('test-endpoint');

        $this->assertEquals(['success' => true], $response);
    }

    public function testPostRequest()
    {
        $data = ['key' => 'value'];

        $this->mockClient->shouldReceive('request')
            ->with('POST', 'test-endpoint', ['json' => $data])
            ->andReturn(new Response(200, [], json_encode(['success' => true])));

        $response = $this->sapService->post('test-endpoint', $data);

        $this->assertEquals(['success' => true], $response);
    }

    public function testClientException()
    {
        $exception = new ClientException('Client error', new \GuzzleHttp\Psr7\Request('GET', 'test-endpoint'));

        $this->mockClient->shouldReceive('request')
            ->with('GET', 'test-endpoint')
            ->andThrow($exception);

        $response = $this->sapService->get('test-endpoint');

        $this->assertEquals(['error' => 'Client error occurred.'], $response);
    }

    public function testServerException()
    {
        $exception = new ServerException('Server error', new \GuzzleHttp\Psr7\Request('GET', 'test-endpoint'));

        $this->mockClient->shouldReceive('request')
            ->with('GET', 'test-endpoint')
            ->andThrow($exception);

        $response = $this->sapService->get('test-endpoint');

        $this->assertEquals(['error' => 'Server error occurred.'], $response);
    }

    public function testConnectException()
    {
        $exception = new ConnectException('Connection error', new \GuzzleHttp\Psr7\Request('GET', 'test-endpoint'));

        $this->mockClient->shouldReceive('request')
            ->with('GET', 'test-endpoint')
            ->andThrow($exception);

        $response = $this->sapService->get('test-endpoint');

        $this->assertEquals(['error' => 'Connection error occurred.'], $response);
    }

    public function testRequestException()
    {
        $exception = new RequestException('Request error', new \GuzzleHttp\Psr7\Request('GET', 'test-endpoint'));

        $this->mockClient->shouldReceive('request')
            ->with('GET', 'test-endpoint')
            ->andThrow($exception);

        $response = $this->sapService->get('test-endpoint');

        $this->assertEquals(['error' => 'Request error occurred.'], $response);
    }

    protected function tearDown(): void
    {
        Mockery::close();
    }
}
