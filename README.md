# SAP ICF Service

A Laravel package for interacting with SAP ICF Service.

## Installation

To install the package, run the following command:

```bash
composer require shakegwapo/sap-icf-service
````

## Configuration
After installing the package, you need to publish the configuration file. Run the following command to publish the configuration file:


Copy code
```bash
php artisan vendor:publish --tag=config
```
This will create a sap.php configuration file in your config directory. You need to set your SAP credentials in the .env file:

env
Copy code
```bash
SAP_BASE_URL=https://your-sap-base-url.com
SAP_USERNAME=your-username
SAP_PASSWORD=your-password
```

## Usage
Injecting the Service
To use the SapService in your application, you need to inject it into your controllers or services. Here is an example of how to do that:

```bash

use Shakegwapo\SapIcfService\SapService;

class YourController extends Controller
{
    protected $sapService;

    public function __construct(SapService $sapService)
    {
        $this->sapService = $sapService;
    }

    public function index()
    {
        $response = $this->sapService->get('your-endpoint');
        // Handle the response
    }
}
```

## Making Requests
GET Request
To make a GET request, you can use the get method of the SapService:

```bash
$response = $this->sapService->get('your-endpoint');
```

## POST Request
To make a POST request, you can use the post method of the SapService:

```bash
$data = [
    'key' => 'value'
];
$response = $this->sapService->post('your-endpoint', $data);

```

## Error Handling
The SapService handles various exceptions and logs the errors. It returns a standardized error message in case of an exception:
```bash
ClientException: Returns 'Client error occurred.'
ServerException: Returns 'Server error occurred.'
ConnectException: Returns 'Connection error occurred.'
RequestException: Returns 'Request error occurred.'
General Exception: Returns 'An error occurred.'
```
