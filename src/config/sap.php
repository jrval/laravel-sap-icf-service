<?php

return [
    'base_url' => env('SAP_BASE_URL', 'https://your-sap-base-url.com'),
    'username' => env('SAP_USERNAME', 'your-username'),
    'password' => env('SAP_PASSWORD', 'your-password'),
];
