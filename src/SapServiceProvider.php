<?php

namespace Shakegwapo\SapIcfService;

use Illuminate\Support\ServiceProvider;

class SapServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/sap.php', 'sap');

        $this->app->singleton(SapService::class, function ($app) {
            return new SapService();
        });
    }

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/config/sap.php' => config_path('sap.php'),
            ], 'config');
        }
    }
}
