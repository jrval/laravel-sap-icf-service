<?php

namespace Shakegwapo\SapIcfService;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ConnectException;

class SapService
{
    protected $client;

    public function __construct()
    {
        $username = config('sap.username');
        $password = config('sap.password');

        $credentials = base64_encode("{$username}:{$password}");

        $this->client = new Client([
            'base_uri' => config('sap.base_url'),
            'headers' => [
                'Authorization' => "Basic {$credentials}",
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ],
            'verify' => true,
        ]);
    }

    public function get($endpoint)
    {
        try {
            $response = $this->client->request('GET', $endpoint);
            return json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $e) {
            Log::error('Client Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'request' => $e->getRequest()->getMethod() . ' ' . $e->getRequest()->getUri(),
                'response' => $e->hasResponse() ? $e->getResponse()->getBody()->getContents() : null
            ]);
            return ['error' => 'Client error occurred.'];
        } catch (ServerException $e) {
            Log::error('Server Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'request' => $e->getRequest()->getMethod() . ' ' . $e->getRequest()->getUri(),
                'response' => $e->hasResponse() ? $e->getResponse()->getBody()->getContents() : null
            ]);
            return ['error' => 'Server error occurred.'];
        } catch (ConnectException $e) {
            Log::error('Connection Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'request' => $e->getRequest()->getMethod() . ' ' . $e->getRequest()->getUri()
            ]);
            return ['error' => 'Connection error occurred.'];
        } catch (RequestException $e) {
            Log::error('Request Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'request' => $e->getRequest()->getMethod() . ' ' . $e->getRequest()->getUri(),
                'response' => $e->hasResponse() ? $e->getResponse()->getBody()->getContents() : null
            ]);
            return ['error' => 'Request error occurred.'];
        } catch (\Exception $e) {
            Log::error('General Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ]);
            return ['error' => 'An error occurred.'];
        }
    }

    public function post($endpoint, $data)
    {
        try {
            $response = $this->client->request('POST', $endpoint, [
                'json' => $data
            ]);
            return json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $e) {
            Log::error('Client Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'request' => $e->getRequest()->getMethod() . ' ' . $e->getRequest()->getUri(),
                'response' => $e->hasResponse() ? $e->getResponse()->getBody()->getContents() : null
            ]);
            return ['error' => 'Client error occurred.'];
        } catch (ServerException $e) {
            Log::error('Server Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'request' => $e->getRequest()->getMethod() . ' ' . $e->getRequest()->getUri(),
                'response' => $e->hasResponse() ? $e->getResponse()->getBody()->getContents() : null
            ]);
            return ['error' => 'Server error occurred.'];
        } catch (ConnectException $e) {
            Log::error('Connection Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'request' => $e->getRequest()->getMethod() . ' ' . $e->getRequest()->getUri()
            ]);
            return ['error' => 'Connection error occurred.'];
        } catch (RequestException $e) {
            Log::error('Request Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'request' => $e->getRequest()->getMethod() . ' ' . $e->getRequest()->getUri(),
                'response' => $e->hasResponse() ? $e->getResponse()->getBody()->getContents() : null
            ]);
            return ['error' => 'Request error occurred.'];
        } catch (\Exception $e) {
            Log::error('General Exception', [
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ]);
            return ['error' => 'An error occurred.'];
        }
    }
}
